# frozen_string_literal: true

class Post < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  index_name Rails.application.class.parent_name.underscore
  document_type name.downcase

  settings index: { number_of_shards: 1 } do
    mapping do
      indexes :content
      indexes :user, type: 'nested' do
        # include_root_in_json true
        indexes :name
      end
    end
  end

  def self.search(query)
    __elasticsearch__.search(
        {
            "query": {
                "bool": {
                    "should": [
                        {
                            "multi_match": {
                                "query": query
                            }
                        },
                        {
                            "nested": {
                                "path": "user",
                                "ignore_unmapped": true,
                                "query": {
                                    "bool": {
                                        "should": [
                                            {
                                                "multi_match": {
                                                    "query": query
                                                }
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    ]
                }
            },
            "highlight": {
                "pre_tags": ["<mark>"],
                "post_tags": ["</mark>"],
                "fields": {
                    "content": {},
                    "user.name": {}
                }
            }
        }
    )
  end

  def as_indexed_json(options = nil)
    self.as_json( include: :user )
  end

  belongs_to :user
  has_many :photos, dependent: :destroy
  has_many :likes, -> { order(created_at: :desc) }

  def is_liked(user)
    Like.find_by(user_id: user.id, post_id: id)
  end
end
