module ApplicationHelper
  def avatar_url user, size = 80
    gravatar_id = Digest::MD5::hexdigest(user.email).downcase
    "https://www.gravatar.com/avatar/#{gravatar_id}.jpg?s=#{size}&r=pg"
  end
end
