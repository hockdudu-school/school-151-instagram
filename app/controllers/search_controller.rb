class SearchController < ApplicationController
  def search
    unless params[:query].blank?
      @results = Post.search(params[:query])
    end
  end
end
