class RegistrationsController < Devise::RegistrationsController

  def edit_password
    @user = current_user
    if request.put?
      if @user.update(user_params)
        # Sign in the user by passing validation in case their password changed
        bypass_sign_in(@user)
        redirect_to root_path
      end
    end
  end

  protected

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
