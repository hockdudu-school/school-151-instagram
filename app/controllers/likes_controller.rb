class LikesController <ApplicationController
  before_action :authenticate_user!

  def create
    post = Post.find(params[:post_id])
    likes = post.likes
    like = likes.build(user_id: current_user.id)
    if like.save
      render json: get_response_object(params[:post_id])
    else
      render plain: 'error', status: :internal_server_error
    end
  end

  def destroy
    post = Post.find(params[:post_id])
    like = Like.find_by(user_id: current_user.id, post_id: post.id)

    if like.destroy
      render json: get_response_object(params[:post_id])
    else
      render plain: 'error', status: :internal_server_error
    end
  end

  private

  def like_params
    params.permit :post_id
  end

  def get_response_object(post_id)
    post = Post.find(post_id)
    likes_text = render_to_string partial: 'posts/like_text', locals: {likes: post.likes}
    user_likes = !Like.find_by(user_id: current_user.id, post_id: post_id).nil?

    {
      likes_count: post.likes.size,
      likes_text: likes_text,
      user_likes: user_likes
    }
  end
end