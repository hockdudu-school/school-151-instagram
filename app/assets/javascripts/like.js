document.addEventListener("turbolinks:load", function() {
    const csrfToken = $('meta[name="csrf-token"]').attr('content');

    $('.like-action').on('click', event => {
        const $target = $(event.target);
        const action = $target.data('action');
        const liked = $target.data('liked');

        const $likedText = $(`.like-text[data-post-id="${$target.data('postId')}"]`);

        $.ajax({
            url: action,
            method: liked ? 'DELETE' : 'POST',
            headers: {
                'X-CSRF-Token': csrfToken
            }
        }).done((data) => {
            $target.data('liked', data.user_likes);
            data.user_likes ? $target.addClass('filled') : $target.removeClass('filled');
            $likedText.html(data.likes_text)
        }).fail((jqXHR) => {
            console.log(jqXHR);
        });
    });
});