document.addEventListener("turbolinks:load", function() {
    Dropzone.autoDiscover = false;

    $('.upload-images').dropzone({
        addRemoveLinks: true,
        maxFileSize: 3,
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        paramName: 'images',
        previewsContainer: '.dropzone-preview',
        clickable: '.upload-photos-icon',
        thumbnailWidth: 100,
        thumbnailHeight: 100,

        init: function() {
            const myDropzone = this;

            $(this.element).find('input[type=submit]').on('click', (e) => {
                e.preventDefault();
                e.stopPropagation();
                if (myDropzone.files.length) {
                    myDropzone.processQueue();
                } else {
                    toastr.error('Please select a file to upload');
                }
            });

            this.on('successmultiple', () => {
                window.location.reload();
            });

            this.on('errormultiple', (files, response) => {
                toastr.error(response);
            })
        }
    });
});