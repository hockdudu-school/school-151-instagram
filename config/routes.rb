Rails.application.routes.draw do
  get 'users/show'
  root to: 'posts#index'
  devise_for :users, controllers: { registrations: 'registrations' }

  as :user do
    get 'users/edit_password' => 'registrations#edit_password', :as => 'edit_user_registration_password'
    put 'users/edit_password' => 'devise/registrations#update'
  end

  get 'search' => 'search#search'

  resources :users, only: [:index, :show]

  resources :posts, only: [:index, :show, :create, :destroy] do
    resources :photos, only: [:create]
  end

  post 'api/like/:post_id' => 'likes#create'
  delete 'api/like/:post_id' => 'likes#destroy'
end